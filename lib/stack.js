var D = module.exports.D = function($stack) {
    $stack.push($stack[$stack.length - 1]);
};

var P = module.exports.P = function($stack) {
    $stack.pop();
};

var S = module.exports.S = function($stack) {
    var tmp = $stack[$stack.length - 2];
    $stack[$stack.length - 2] = $stack[$stack.length - 1];
    $stack[$stack.length - 1] = tmp;
};
