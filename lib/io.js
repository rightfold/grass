var log = module.exports.log = function($stack) {
    var value = $stack.pop();
    $stack.push(function() {
        console.log(value);
    });
};
