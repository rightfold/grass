module Grassc.Opt.Uncat
( uncat
) where

import Grassc.AST

uncat :: Module a -> Module a
uncat = uncatModule

uncatModule :: Module a -> Module a
uncatModule (Module defs) = Module (map uncatDef defs)

uncatDef :: Def a -> Def a
uncatDef (ValueDef name v)       = ValueDef name (uncatValueExpr v)
uncatDef (InlineValueDef name v) = InlineValueDef name (uncatValueExpr v)
uncatDef d = d

uncatValueExpr :: ValueExpr a -> ValueExpr a
uncatValueExpr (VarValueExpr name t) = VarValueExpr name t
uncatValueExpr (CatValueExpr [v]) = uncatValueExpr v
uncatValueExpr (CatValueExpr vs) = CatValueExpr (map uncatValueExpr vs)
uncatValueExpr (LamValueExpr v) = LamValueExpr (uncatValueExpr v)
uncatValueExpr (ForeignValueExpr url t) = ForeignValueExpr url t
uncatValueExpr (StringValueExpr value) = StringValueExpr value
