{-# LANGUAGE DeriveFunctor #-}
module Grassc.AST where

newtype Module t = Module [Def t] deriving (Eq, Show, Functor)

data Def t
  = ValueDef String (ValueExpr t)
  | InlineValueDef String (ValueExpr t)
  | DataDef String [(String, [TypeExpr t])]
  | PicDef String (TypeExpr t)
  deriving (Eq, Show, Functor)

data ValueExpr t
  = VarValueExpr String t
  | CatValueExpr [ValueExpr t]
  | LamValueExpr (ValueExpr t)
  | ForeignValueExpr (String, String, String) (TypeExpr t)
  | StringValueExpr String
  deriving (Eq, Show, Functor)

data TypeExpr t
  = VarTypeExpr String t
  | FunTypeExpr [TypeExpr t] [TypeExpr t]
  deriving (Eq, Show, Functor)
