module Grassc.Lex
( space

, identifier

, dataKeyword
, defKeyword
, foreignKeyword
, inlineKeyword
, picKeyword

, stringLiteral

, leftBrace
, rightBrace
, leftBracket
, rightBracket
, leftParen
, rightParen
, thinArrow
, bang
) where

import Control.Applicative ((<|>))
import Control.Monad (when)
import Text.Parsec ( (<?>)
                   , char
                   , eof
                   , many
                   , noneOf
                   , notFollowedBy
                   , oneOf
                   , string
                   , unexpected
                   )
import Text.Parsec.String (Parser)

lexeme :: Parser a -> Parser a
lexeme p = p <* many space

space :: Parser ()
space = white <|> comment
  where white = oneOf "\x09\x0A\x0B\x0C\x0D\x20" >> return ()
        comment = do
          char '%'
          many (noneOf "\n")
          eof <|> (char '\n' >> return ())

identifier :: Parser String
identifier = lexeme . (<?> "identifier") $ do
  name <- (:) <$> identifierHead <*> many identifierTail
  when (name `elem` ["data", "def", "foreign", "inline", "pic"]) (unexpected name)
  return name

identifierHead, identifierTail :: Parser Char
identifierHead = oneOf $ ['a'..'z'] ++ ['A'..'Z'] ++ "_~"
identifierTail = identifierHead <|> oneOf ['0'..'9']

keyword :: String -> Parser ()
keyword s = lexeme $ string s >> notFollowedBy identifierTail

dataKeyword, defKeyword, foreignKeyword, inlineKeyword, picKeyword :: Parser ()
dataKeyword    = keyword "data"
defKeyword     = keyword "def"
foreignKeyword = keyword "foreign"
inlineKeyword  = keyword "inline"
picKeyword     = keyword "pic"

stringLiteral :: Parser String
stringLiteral = lexeme $ char '"' *> many (noneOf "\"") <* char '"'

punct :: String -> Parser ()
punct s = lexeme $ string s >> return ()

leftBrace, rightBrace, leftBracket, rightBracket :: Parser ()
leftParen, rightParen, thinArrow, bang :: Parser ()
leftBrace    = punct "{"
rightBrace   = punct "}"
leftBracket  = punct "["
rightBracket = punct "]"
leftParen    = punct "("
rightParen   = punct ")"
thinArrow    = punct "->"
bang         = punct "!"
