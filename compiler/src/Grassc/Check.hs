module Grassc.Check
( CheckError(..)
, check
) where

import Control.Monad (join, when)
import Control.Monad.Error.Class (throwError)
import Control.Monad.State (evalStateT, get, put, StateT)
import Data.Map (Map)
import qualified Data.Map as Map
import Grassc.AST
import Grassc.Type (DataTypeID(..), Type(..), TypeVar(..), UniVar(..))

data CheckError
  = ValueNotInScope String
  | TypeNotInScope String
  | TypeMismatch Type Type
  deriving (Eq, Show)

data S = S { sID :: Int
           , sSolved :: Map UniVar Type
           , sE :: E
           }

type Check = StateT S (Either CheckError)

newID :: Check Int
newID = do
  s@S { sID = id } <- get
  put $ s { sID = id + 1 }
  return id

uniVar :: Check UniVar
uniVar = UniVar <$> newID

typeVar :: Check TypeVar
typeVar = TypeVar <$> newID

dataTypeID :: Check DataTypeID
dataTypeID = DataTypeID <$> newID

solve :: UniVar -> Type -> Check ()
solve u t = do
  s@S { sSolved = solved } <- get
  put $ s { sSolved = Map.insert u t solved }

prune :: Type -> Check Type
prune (UniVarType v) = do
  S { sSolved = solved } <- get
  case Map.lookup v solved of
    Just t  -> prune t
    Nothing -> return $ UniVarType v
prune (FunctionType from to) =
  FunctionType <$> mapM prune from <*> mapM prune to
prune UnitType = return UnitType
prune BoolType = return BoolType
prune IntType = return IntType
prune StringType = return StringType
prune (IOType t) = IOType <$> prune t
prune (DataType id) = return (DataType id)
prune (ForallType v t) = ForallType v <$> prune t
prune (TypeVarType v) = return $ TypeVarType v

pruneModule :: Module Type -> Check (Module Type)
pruneModule (Module defs) = Module <$> mapM pruneDef defs

pruneDef :: Def Type -> Check (Def Type)
pruneDef d@(DataDef _ _)      = return d
pruneDef (ValueDef name v)    = ValueDef name <$> pruneValueExpr v
pruneDef (InlineValueDef _ _) = error "inline value defs should have been eliminated"
pruneDef d@(PicDef _ _)       = return d

pruneValueExpr :: ValueExpr Type -> Check (ValueExpr Type)
pruneValueExpr (VarValueExpr name t) = VarValueExpr name <$> prune t
pruneValueExpr (CatValueExpr vs) = CatValueExpr <$> mapM pruneValueExpr vs
pruneValueExpr (LamValueExpr v) = LamValueExpr <$> pruneValueExpr v
pruneValueExpr (ForeignValueExpr url t) = ForeignValueExpr url <$> pruneTypeExpr t
pruneValueExpr (StringValueExpr value) = return $ StringValueExpr value

pruneTypeExpr :: TypeExpr Type -> Check (TypeExpr Type)
pruneTypeExpr (VarTypeExpr name t) = VarTypeExpr name <$> prune t
pruneTypeExpr (FunTypeExpr from to) = FunTypeExpr <$> mapM pruneTypeExpr from
                                                  <*> mapM pruneTypeExpr to

data ValueSym
  = ValueDefSym Type
  | InlineValueDefSym (ValueExpr ())

data TypeSym
  = TypeSym Type

data E = E (Map String ValueSym) (Map String TypeSym)

getValueSym :: String -> Check ValueSym
getValueSym name = do
  S { sE = (E vs _) } <- get
  case Map.lookup name vs of
    Just t  -> return t
    Nothing -> throwError (ValueNotInScope name)

putValueSym :: String -> ValueSym -> Check ()
putValueSym name type_ = do
  s@S { sE = (E vs ts) } <- get
  put $ s { sE = E (Map.insert name type_ vs) ts }

getTypeSym :: String -> Check TypeSym
getTypeSym name = do
  S { sE = (E _ ts) } <- get
  case Map.lookup name ts of
    Just t  -> return t
    Nothing -> throwError (TypeNotInScope name)

putTypeSym :: String -> TypeSym -> Check ()
putTypeSym name type_ = do
  s@S { sE = (E vs ts) } <- get
  put $ s { sE = E vs (Map.insert name type_ ts) }

check :: Module () -> Either CheckError (Module Type)
check m = flip evalStateT (S 0 Map.empty (E Map.empty Map.empty)) $ do
  putTypeSym "bool" (TypeSym BoolType)
  putTypeSym "int" (TypeSym IntType)
  putTypeSym "string" (TypeSym StringType)

  -- TEMPORARY
  putTypeSym "__io_unit" (TypeSym (IOType UnitType))

  a <- typeVar; putValueSym "D" (ValueDefSym (ForallType a (FunctionType [TypeVarType a] [TypeVarType a, TypeVarType a])))
  a <- typeVar; putValueSym "P" (ValueDefSym (ForallType a (FunctionType [TypeVarType a] [])))
  a <- typeVar; b <- typeVar; putValueSym "S" (ValueDefSym (ForallType a (ForallType b (FunctionType [TypeVarType a, TypeVarType b] [TypeVarType b, TypeVarType a]))))

  a <- typeVar; b <- typeVar; putValueSym "bind" (ValueDefSym (ForallType a (ForallType b (FunctionType [IOType (TypeVarType a), FunctionType [TypeVarType a] [IOType (TypeVarType b)]] [IOType (TypeVarType b)]))))

  a <- typeVar; putValueSym "print" (ValueDefSym (ForallType a (FunctionType [TypeVarType a] [IOType UnitType])))

  putValueSym "~" (ValueDefSym (FunctionType [StringType, StringType] [StringType]))

  putValueSym "__int_add" (ValueDefSym (FunctionType [IntType, IntType] [IntType]))
  putValueSym "__int_sub" (ValueDefSym (FunctionType [IntType, IntType] [IntType]))
  putValueSym "__int_mul" (ValueDefSym (FunctionType [IntType, IntType] [IntType]))
  putValueSym "__int_div" (ValueDefSym (FunctionType [IntType, IntType] [IntType]))
  putValueSym "__int_neg" (ValueDefSym (FunctionType [IntType] [IntType]))

  putValueSym "__bool_and" (ValueDefSym (FunctionType [BoolType, BoolType] [BoolType]))
  putValueSym "__bool_or" (ValueDefSym (FunctionType [BoolType, BoolType] [BoolType]))
  putValueSym "__bool_not" (ValueDefSym (FunctionType [BoolType] [BoolType]))

  checkModule m >>= pruneModule

checkModule :: Module () -> Check (Module Type)
checkModule (Module defs) = Module . join <$> mapM checkDef defs

checkDef :: Def () -> Check [Def Type]
checkDef (DataDef name variants) = do
  variants' <- flip mapM variants $ \(name, params) -> do
    (,) name <$> mapM checkTypeExpr params
  t <- DataType <$> dataTypeID
  flip mapM variants' $ \(name, params) -> do
    paramTypes <- mapM typeExprType params
    putValueSym name (ValueDefSym (FunctionType paramTypes [t]))
  putTypeSym name (TypeSym t)
  return [DataDef name variants']
checkDef (ValueDef name value) = do
  -- TODO: Recursive definitions.
  value' <- checkValueExpr value
  putValueSym name =<< ValueDefSym <$> valueExprType value'
  return [ValueDef name value']
checkDef (InlineValueDef name value) = do
  putValueSym name (InlineValueDefSym value)
  return []
checkDef (PicDef name t) = do
  t' <- checkTypeExpr t
  return [PicDef name t']

checkValueExpr :: ValueExpr () -> Check (ValueExpr Type)
checkValueExpr (VarValueExpr name ()) = do
  sym <- getValueSym name
  case sym of
    ValueDefSym t       -> return $ VarValueExpr name t
    InlineValueDefSym v -> checkValueExpr v
checkValueExpr (CatValueExpr vs) =
  CatValueExpr <$> mapM checkValueExpr vs
checkValueExpr (LamValueExpr v) =
  LamValueExpr <$> checkValueExpr v
checkValueExpr (ForeignValueExpr url t) =
  ForeignValueExpr url <$> checkTypeExpr t
checkValueExpr (StringValueExpr v) =
  return $ StringValueExpr v

checkTypeExpr :: TypeExpr () -> Check (TypeExpr Type)
checkTypeExpr (VarTypeExpr name ()) = do
  TypeSym t <- getTypeSym name
  return $ VarTypeExpr name t
checkTypeExpr (FunTypeExpr from to) =
  FunTypeExpr <$> mapM checkTypeExpr from <*> mapM checkTypeExpr to

valueExprType :: ValueExpr Type -> Check Type
valueExprType = (prune =<<) . go
  where go (VarValueExpr _ t) = return t
        go (CatValueExpr vs) = ho vs [] []
          where ho :: [ValueExpr Type] -> [Type] -> [Type] -> Check Type
                ho []       from to = return $ FunctionType from to
                ho (v : vs) from to = do
                  FunctionType tfrom tto <- skolemize =<< go v
                  let from' = take (length tfrom - length to   ) tfrom ++ from
                  let to'   = take (length to    - length tfrom) to    ++ tto
                  let stack = from' ++ to in mapM (uncurry unify) (stack `zip` tfrom)
                  ho vs from' to'

                unify :: Type -> Type -> Check ()
                unify = \t u -> do { t' <- prune t; u' <- prune u; go t' u' }
                  where go t u | t == u          = return ()
                        go (UniVarType a) u      = solve a u
                        go t (UniVarType b)      = solve b t
                        go t@(FunctionType tfs tts) u@(FunctionType ufs uts) = do
                          when ( length tfs /= length ufs
                              || length tts /= length uts ) $ do
                            throwError $ TypeMismatch t u
                          mapM_ (uncurry go) (tfs `zip` ufs)
                          mapM_ (uncurry go) (tts `zip` uts)
                        go (IOType a) (IOType b) = a `go` b
                        go t u                   = throwError $ TypeMismatch t u
        go (LamValueExpr v) =
          (\t -> FunctionType [] [t]) <$> go v
        go (ForeignValueExpr _ t) = typeExprType t
        go (StringValueExpr _) = return $ FunctionType [] [StringType]

typeExprType :: TypeExpr Type -> Check Type
typeExprType = (prune =<<) . go
  where go (VarTypeExpr _ t)     = return t
        go (FunTypeExpr from to) = FunctionType <$> mapM go from <*> mapM go to

skolemize :: Type -> Check Type
skolemize = go Map.empty
  where go :: Map TypeVar UniVar -> Type -> Check Type
        go _ (UniVarType v)         = return $ UniVarType v
        go m (FunctionType from to) = FunctionType <$> mapM (go m) from
                                                   <*> mapM (go m) to
        go _ UnitType               = return UnitType
        go _ BoolType               = return BoolType
        go _ IntType                = return IntType
        go _ StringType             = return StringType
        go m (IOType t)             = IOType <$> go m t
        go _ (DataType id)          = return (DataType id)
        go m (ForallType v t)       = do { v' <- uniVar; go (Map.insert v v' m) t }
        go m (TypeVarType v)        = return $ case Map.lookup v m of
                                        Just uv -> UniVarType uv
                                        Nothing -> TypeVarType v
