module Grassc.Parse
( parse
) where

import Control.Applicative ((<|>))
import Data.List.Split (splitOn)
import Data.Maybe (isJust)
import Grassc.AST
import Grassc.Lex
import Text.Parsec (eof, many, optionMaybe, ParseError, try)
import qualified Text.Parsec as Parsec
import Text.Parsec.String (Parser)

parse :: String -> String -> Either ParseError (Module ())
parse = Parsec.parse (many space *> module_ <* eof)

module_ :: Parser (Module ())
module_ = Module <$> many def

def :: Parser (Def ())
def = try dataDef <|> valueDef <|> picDef

dataDef :: Parser (Def ())
dataDef = do
  dataKeyword
  name <- identifier
  leftParen
  variants <- many $ do
    params <- leftParen *> many primaryTypeExpr <* rightParen
    name <- identifier
    return (name, params)
  rightParen
  return $ DataDef name variants

valueDef :: Parser (Def ())
valueDef = do
  isInline <- isJust <$> optionMaybe inlineKeyword
  defKeyword
  name <- identifier
  value <- valueExpr
  return $ (if isInline then InlineValueDef else ValueDef) name value

picDef :: Parser (Def ())
picDef = do
  picKeyword
  name <- identifier
  type_ <- primaryTypeExpr
  return $ PicDef name type_

valueExpr :: Parser (ValueExpr ())
valueExpr = try varValueExpr
        <|> foreignValueExpr
        <|> catValueExpr
        <|> actionCatValueExpr
        <|> lamValueExpr
        <|> stringValueExpr

varValueExpr :: Parser (ValueExpr ())
varValueExpr = flip VarValueExpr () <$> identifier

foreignValueExpr :: Parser (ValueExpr ())
foreignValueExpr = do
  foreignKeyword
  [callingConv, path, name] <- splitOn ":" <$> stringLiteral
  type_ <- primaryTypeExpr
  return $ ForeignValueExpr (callingConv, path, name) type_

catValueExpr :: Parser (ValueExpr ())
catValueExpr = CatValueExpr <$> (leftParen *> many valueExpr <* rightParen)

actionCatValueExpr :: Parser (ValueExpr ())
actionCatValueExpr = CatValueExpr <$> (leftBracket *> body <* rightBracket)
  where body = binding <|> normal <|> return []
        binding = do
          action <- bang *> valueExpr
          rest <- body
          return [ action
                 , LamValueExpr (CatValueExpr rest)
                 , VarValueExpr "bind" ()
                 ]
        normal = do
          value <- valueExpr
          rest <- body
          return (value : rest)

lamValueExpr :: Parser (ValueExpr ())
lamValueExpr =
  LamValueExpr . CatValueExpr <$> (leftBrace *> many valueExpr <* rightBrace)

stringValueExpr :: Parser (ValueExpr ())
stringValueExpr = StringValueExpr <$> stringLiteral

typeExpr :: Parser (TypeExpr ())
typeExpr = try funTypeExpr <|> primaryTypeExpr

funTypeExpr :: Parser (TypeExpr ())
funTypeExpr = do
  from <- many primaryTypeExpr
  thinArrow
  to <- many primaryTypeExpr
  return $ FunTypeExpr from to

primaryTypeExpr :: Parser (TypeExpr ())
primaryTypeExpr = varTypeExpr <|> parenedTypeExpr

varTypeExpr :: Parser (TypeExpr ())
varTypeExpr = flip VarTypeExpr () <$> identifier

parenedTypeExpr :: Parser (TypeExpr ())
parenedTypeExpr = leftParen *> typeExpr <* rightParen
