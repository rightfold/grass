module Grassc.ECMAScript
( toECMAScript
) where

import Data.Char (ord)
import Grassc.AST
import Grassc.Type (Type)
import Language.ECMAScript3.Syntax

toECMAScript :: Module Type -> JavaScript ()
toECMAScript = moduleToECMAScript

moduleToECMAScript :: Module Type -> JavaScript ()
moduleToECMAScript (Module defs) = Script () (defs >>= defToECMAScript)

defToECMAScript :: Def Type -> [Statement ()]
defToECMAScript (DataDef _ variants)    = map variant variants
  where variant (name, params) = FunctionStmt () (Id () (mangle name))
                                                 [stack']
                                                 (body name $ length params)
        body nm n = [VarDeclStmt () [VarDecl () object' (Just $ ObjectLit () [])]]
                 ++ [tag nm]
                 ++ map prop (reverse [0..n-1])
                 ++ [push object]
        tag nm    = ExprStmt () (AssignExpr () OpAssign (LDot () object "tag") (StringLit () nm))
        prop i    = ExprStmt () (AssignExpr () OpAssign (LDot () object ("_" ++ show i)) pop)
        object    = VarRef () object'
        object'   = Id () "$object"
defToECMAScript (ValueDef name value)   = [VarDeclStmt () [varDecl]]
  where varDecl = VarDecl () (Id () (mangle name))
                             (Just $ valueExprToECMAScript value)
defToECMAScript (InlineValueDef _ _)    =
  error "inline value defs should have been eliminated"
defToECMAScript (PicDef _ _)            = []

valueExprToECMAScript :: ValueExpr Type -> Expression ()
valueExprToECMAScript (VarValueExpr name _) = VarRef () (Id () (mangle name))
valueExprToECMAScript (CatValueExpr fs) = FuncExpr () Nothing [stack'] stmts
  where stmts  = map (ExprStmt ()) exprs
        exprs  = map expr fs
        expr f = CallExpr () (valueExprToECMAScript f) [stack]
valueExprToECMAScript (LamValueExpr v) = FuncExpr () Nothing [stack'] stmts
  where stmts = [push (valueExprToECMAScript v)]
valueExprToECMAScript (ForeignValueExpr locator _) =
  case locator of
    ("esstack", path, name) ->
      let require = CallExpr () (VarRef () (Id () "require")) [StringLit () path]
       in DotRef () require (Id () name)
    _ -> error "NYI"
valueExprToECMAScript (StringValueExpr v) = FuncExpr () Nothing [stack'] stmts
  where stmts = [push (StringLit () v)]

stack :: Expression ()
stack = VarRef () stack'

stack' :: Id ()
stack' = (Id () "$stack")

pop :: Expression ()
pop = CallExpr () (DotRef () stack (Id () "pop")) []

push :: Expression () -> Statement ()
push e = ExprStmt () (CallExpr () (DotRef () stack (Id () "push")) [e])

mangle :: String -> String
mangle s = s >>= mc
  where mc c | safe c = [c]
             | otherwise = "$" ++ show (ord c)
        safe c = (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_'
