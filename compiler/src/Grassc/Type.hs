module Grassc.Type where

newtype UniVar = UniVar Int deriving (Eq, Ord, Show)
newtype TypeVar = TypeVar Int deriving (Eq, Ord, Show)
newtype DataTypeID = DataTypeID Int deriving (Eq, Ord, Show)

data Type
  = UniVarType UniVar
  | FunctionType [Type] [Type]
  | UnitType    -- TODO: Generalize tuple types.
  | BoolType
  | IntType
  | StringType
  | IOType Type -- TODO: Generalize type constructors.
  | DataType DataTypeID
  | ForallType TypeVar Type
  | TypeVarType TypeVar
  deriving (Eq, Show)

-- TODO: Fix Eq Type WRT forall

formatType :: Type -> String
formatType (UniVarType (UniVar id)) = "#" ++ show id
formatType (FunctionType from to) = "(" ++ ffrom ++ "->" ++ fto ++ ")"
  where ffrom = from >>= (++ " ") . formatType
        fto   = to   >>= (" " ++) . formatType
formatType UnitType = "{}"
formatType BoolType = "bool"
formatType IntType = "int"
formatType StringType = "string"
formatType (IOType t) = "(" ++ formatType t ++ " io)"
formatType (ForallType (TypeVar id) t) = "forall $" ++ show id ++ ". " ++ formatType t
formatType (TypeVarType (TypeVar id)) = "$" ++ show id
