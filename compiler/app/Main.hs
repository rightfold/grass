module Main (main) where

import Grassc.Parse (parse)
import Grassc.Check (check)
import Grassc.ECMAScript (toECMAScript)
import Grassc.Opt.Uncat (uncat)
import Language.ECMAScript3.PrettyPrint (javaScript)
import System.Environment (getArgs, getProgName)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [filename] -> compile filename
    _ -> usage

compile :: String -> IO ()
compile filename = do
  code <- readFile filename
  let untypedAST = uncat <$> parse "main.grass" code
  case untypedAST of
    Right untypedAST -> do
      let typedAST = check untypedAST
      case typedAST of
        Right typedAST -> print . javaScript $ toECMAScript typedAST
        Left err -> print err
    Left err -> print err

usage :: IO ()
usage = do
  progName <- getProgName
  putStrLn $ "Usage: " ++ progName ++ " <source-file>"
