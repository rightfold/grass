module Grassc.TypeSpec (spec) where

import Grassc.Type
import Test.Hspec (describe, it, shouldBe, Spec)

spec :: Spec
spec = do
  describe "formatType" $ do
    it "formats function types" $ do
      formatType (FunctionType [] []) `shouldBe` "(->)"
      formatType (FunctionType [IntType] []) `shouldBe` "(int ->)"
      formatType (FunctionType [BoolType, IntType] []) `shouldBe` "(bool int ->)"
      formatType (FunctionType [] [BoolType, IntType]) `shouldBe` "(-> bool int)"
      formatType (FunctionType [IntType] [IntType]) `shouldBe` "(int -> int)"
      formatType (FunctionType [FunctionType [] []]
                               [FunctionType [] []])
        `shouldBe` "((->) -> (->))"
