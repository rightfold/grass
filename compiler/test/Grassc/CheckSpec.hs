module Grassc.CheckSpec (spec) where

import Grassc.AST
import Grassc.Check
import Grassc.Parse (parse)
import Grassc.Type (Type(..), TypeVar(..))
import Test.Hspec (describe, it, shouldBe, Spec)

spec :: Spec
spec = do
  describe "check" $ do
    it "checks nothing" $ do
      let Right m = parse "main.grass" ""
      check m `shouldBe` Right (Module [])

    it "checks no-ops" $ do
      let Right m = parse "main.grass" "def main ()"
      check m `shouldBe` Right (Module [ValueDef "main" (CatValueExpr [])])

    it "finds type errors" $ do
      let Right m = parse "main.grass" "def f (__int_neg __bool_not)"
      check m `shouldBe` Left (TypeMismatch IntType BoolType)

    it "checks polymorphism" $ do
      let Right m = parse "main.grass" "def f (__int_add D __int_neg)\ndef g f"
      check m `shouldBe` Right (Module [ ValueDef "f" (CatValueExpr [ VarValueExpr "__int_add" (FunctionType [IntType, IntType] [IntType])
                                                                    , VarValueExpr "D" (let v = TypeVar 0 in ForallType v (FunctionType [TypeVarType v] [TypeVarType v, TypeVarType v]))
                                                                    , VarValueExpr "__int_neg" (FunctionType [IntType] [IntType])
                                                                    ])
                                       , ValueDef "g" (VarValueExpr "f" (FunctionType [IntType, IntType] [IntType, IntType]))
                                       ])

    it "finds type errors in polymorphism" $ do
      let Right m = parse "main.grass" "def f (__int_neg D __bool_not)"
      check m `shouldBe` Left (TypeMismatch IntType BoolType)

    it "checks foreign value exprs" $ do
      let Right m = parse "main.grass" "def f foreign \"cdecl:foo:bar\" (int -> bool)\ndef g f"
      check m `shouldBe` Right (Module [ ValueDef "f" (ForeignValueExpr ("cdecl", "foo", "bar")
                                                                        (FunTypeExpr [VarTypeExpr "int" IntType]
                                                                                     [VarTypeExpr "bool" BoolType]))
                                       , ValueDef "g" (VarValueExpr "f" (FunctionType [IntType] [BoolType]))
                                       ])

    it "inlines" $ do
      let Right m = parse "main.grass" "inline def f (__int_add)\ndef g f\ndef h f"
      check m `shouldBe` Right (Module [ ValueDef "g" (CatValueExpr [VarValueExpr "__int_add" (FunctionType [IntType, IntType] [IntType])])
                                       , ValueDef "h" (CatValueExpr [VarValueExpr "__int_add" (FunctionType [IntType, IntType] [IntType])])
                                       ])

    it "checks simple stuff" $ do
      let Right m = parse "main.grass" "def f ()\ndef g ()\ndef h (f g)\ndef i (h)"
      check m `shouldBe` Right (Module [ ValueDef "f" (CatValueExpr [])
                                       , ValueDef "g" (CatValueExpr [])
                                       , ValueDef "h" (CatValueExpr [ VarValueExpr "f" (FunctionType [] [])
                                                                    , VarValueExpr "g" (FunctionType [] [])
                                                                    ])
                                       , ValueDef "i" (CatValueExpr [VarValueExpr "h" (FunctionType [] [])])
                                       ])

    it "checks complex stuff" $ do
      let Right m = parse "main.grass" "def f (__int_add __int_neg)\ndef g (f)"
      check m `shouldBe` Right (Module [ ValueDef "f" (CatValueExpr [ VarValueExpr "__int_add" (FunctionType [IntType, IntType] [IntType])
                                                                    , VarValueExpr "__int_neg" (FunctionType [IntType] [IntType])
                                                                    ])
                                       , ValueDef "g" (CatValueExpr [VarValueExpr "f" (FunctionType [IntType, IntType] [IntType])])
                                       ])

      let Right m = parse "main.grass" "def f (__int_neg __int_mul)\ndef g (f)"
      check m `shouldBe` Right (Module [ ValueDef "f" (CatValueExpr [ VarValueExpr "__int_neg" (FunctionType [IntType] [IntType])
                                                                    , VarValueExpr "__int_mul" (FunctionType [IntType, IntType] [IntType])
                                                                    ])
                                       , ValueDef "g" (CatValueExpr [VarValueExpr "f" (FunctionType [IntType, IntType] [IntType])])
                                       ])
