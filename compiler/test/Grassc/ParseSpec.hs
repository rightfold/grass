module Grassc.ParseSpec (spec) where

import Grassc.AST
import Grassc.Parse
import Test.Hspec (describe, it, shouldBe, Spec)

spec :: Spec
spec = do
  describe "parse" $ do
    it "parses nothing" $ do
      parse "main.grass" "" `shouldBe` Right (Module [])

    it "parses space" $ do
      parse "main.grass" " \t\r\n" `shouldBe` Right (Module [])

    it "parses comments" $ do
      parse "main.grass" "%\n" `shouldBe` Right (Module [])
      parse "main.grass" "% \n" `shouldBe` Right (Module [])
      parse "main.grass" "%" `shouldBe` Right (Module [])
      parse "main.grass" "% " `shouldBe` Right (Module [])

    it "parses var value exprs" $ do
      let expected = Module [ValueDef "main" (VarValueExpr "main2" ())]
      parse "main.grass" "def main main2" `shouldBe` Right expected

    it "parses cat value exprs" $ do
      let expected = Module [ValueDef "main" (CatValueExpr [ VarValueExpr "main2" ()
                                                           , VarValueExpr "main3" ()
                                                           ])]
      parse "main.grass" "def main (main2 main3)" `shouldBe` Right expected

    it "parses action cat value exprs" $ do
      let expected = Module [ValueDef "main" (CatValueExpr [ VarValueExpr "x" ()
                                                           , VarValueExpr "a" ()
                                                           , LamValueExpr (CatValueExpr [ VarValueExpr "y" ()
                                                                                        , VarValueExpr "b" ()
                                                                                        , LamValueExpr (CatValueExpr [VarValueExpr "c" ()])
                                                                                        , VarValueExpr "bind" ()
                                                                                        ])
                                                           , VarValueExpr "bind" ()
                                                           ])]
      parse "main.grass" "def main [x !a y !b c]" `shouldBe` Right expected

    it "parses lam value exprs" $ do
      let expected = Module [ValueDef "main" (LamValueExpr (CatValueExpr [ VarValueExpr "main2" ()
                                                                         , VarValueExpr "main3" ()
                                                                         ]))]
      parse "main.grass" "def main {main2 main3}" `shouldBe` Right expected

    it "parses foreign value exprs" $ do
      let expected = Module [ValueDef "main" (ForeignValueExpr ("cdecl", "foo", "bar")
                                                               (FunTypeExpr [VarTypeExpr "int" ()]
                                                                            [VarTypeExpr "bool" ()]))]
      parse "main.grass" "def main foreign \"cdecl:foo:bar\" (int -> bool)" `shouldBe` Right expected

    it "parses a program" $ do
      let expected = Module [ ValueDef "main" (CatValueExpr [ VarValueExpr "read" ()
                                                            , VarValueExpr "square" ()
                                                            , VarValueExpr "print" ()
                                                            ])
                            , ValueDef "square" (CatValueExpr [ VarValueExpr "dup" ()
                                                              , VarValueExpr "mul" ()
                                                              ])
                            ]
      parse "main.grass" "% Reads an integer from standard input and prints its square.\n\
                         \def main (\n\
                         \    read   % reads from stdin\n\
                         \    square % defined below\n\
                         \    print  % prints result\n\
                         \)\n\
                         \\n\
                         \def square (\n\
                         \    dup % duplicates top entry\n\
                         \    mul % multiplies top entries\n\
                         \)\n" `shouldBe` Right expected
